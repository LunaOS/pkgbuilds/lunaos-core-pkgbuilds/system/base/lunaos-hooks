# LunaOS Hooks

Pacman hooks for setting up certain system identification files of LunaOS.

File name | Description
:--- | :---
lunaos-hooks-runner | Fixes files like `os-release`, `lsb-release`, `issues`, `arch-version`, `firefox`, `fish`, `zsh` for LunaOS.
lunaos-hooks.hook | Runs `lunaos-hooks-runner` after the `lunaos-hooks` package is updated.
lunaos-lsb-release.hook | Runs `lunaos-hooks-runner` after package `lsb-release` has been updated.
lunaos-os-release.hook | Runs `lunaos-hooks-runner` after package `filesystem` has been updated.
lunaos-arch-clean.hook | Runs `lunaos-hooks-runner` after package `filesystem` has been updated.
lunaos-setting.hook | Runs `lunaos-hooks-runner` after package `lunaos-setting` has been updated.
lunaos-firefox.hook | Runs `lunaos-hooks-runner` after package `firefox` has been updated.
lunaos-bash.hook | Runs `lunaos-hooks-runner` after package `bash` has been updated.
lunaos-fish.hook | Runs `lunaos-hooks-runner` after package `fish` has been updated.
lunaos-zsh.hook | Runs `lunaos-hooks-runner` after package `zsh` has been updated.
refind.hook | Runs `refind-install` after package `refind` has been updated.
10-lunaos-coredump.hook | Drop duplicate coredump line from /usr/lib/tmpfiles.d/systemd.conf
lunaos-reboot-required.hook | Notify to reboot required after any listed essential system files have been updated.
pkgfile.hook | Runs `pkgfile --update` after every package update.
blocked-packages.hook | Block broken packagekit install
95-systemd-boot.hook | Runs `systemctl restart systemd-boot-update` after package `systemd` has been updated.


## This repository uses code from

- [Garuda Hooks](https://gitlab.com/garuda-linux/pkgbuilds/-/tree/main/garuda-hooks)
- [EndevourOS Hooks](https://github.com/endeavouros-team/PKGBUILDS/tree/master/eos-hooks)
- [CachyOS Hooks](https://github.com/CachyOS/cachyos-hooks)
- [Systemd-boot ArchWiki](https://wiki.archlinux.org/title/Systemd-boot)
